<?php
//field_landing_path:
echo "create field_landing_path\n";

field_create_field(array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'attributes' => array(
          'target' => 'default',
          'class' => '',
          'rel' => '',
          ),
        'url' => 0,
        'title' => 'optional',
        'title_value' => '',
        'enable_tokens' => 1,
        'display' => array(
          'url_cutoff' => 80,
          ),
        ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_landing_path' => array(
                'url' => 'field_landing_path_url',
                'title' => 'field_landing_path_title',
                'attributes' => 'field_landing_path_attributes',
                ),
              ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_landing_path' => array(
                'url' => 'field_landing_path_url',
                'title' => 'field_landing_path_title',
                'attributes' => 'field_landing_path_attributes',
                ),
              ),
            ),
          ),
        ),
        'foreign keys' => array(),
        'indexes' => array(),
        'field_name' => 'field_landing_path',
        'type' => 'link_field',
        'module' => 'link',
        'active' => '1',
        'locked' => '0',
        'cardinality' => '1',
        'deleted' => '0',
        'columns' => array(
            'url' => array(
              'type' => 'varchar',
              'length' => 2048,
              'not null' => FALSE,
              'sortable' => TRUE,
              ),
            'title' => array(
              'type' => 'varchar',
              'length' => 255,
              'not null' => FALSE,
              'sortable' => TRUE,
              ),
            'attributes' => array(
              'type' => 'text',
              'size' => 'medium',
              'not null' => FALSE,
              ),
            ),
        'bundles' => array(
            'node' => array(
              'xauth_path',
              ),
            ),
        ));
echo "create field_landing_path instance \n";
field_create_instance(array(
      'label' => 'Landing Path',
      'widget' => array(
        'weight' => '1',
        'type' => 'link_field',
        'module' => 'link',
        'active' => 0,
        'settings' => array(),
        ),
      'settings' => array(
        'url' => 0,
        'title' => 'none',
        'title_value' => '',
        'display' => array(
          'url_cutoff' => '80',
          ),
        'attributes' => array(
          'target' => 'default',
          'rel' => '',
          'class' => '',
          ),
        'enable_tokens' => 1,
        'user_register_form' => FALSE,
        ),
      'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'default',
            'settings' => array(),
            'module' => 'link',
            'weight' => 5,
            ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
            ),
          ),
      'required' => 1,
      'description' => '',
      'default_value' => NULL,
      'field_name' => 'field_landing_path',
      'entity_type' => 'node',
      'bundle' => 'xauth_path',
      'deleted' => '0',
      ));
//field_failure_redirect:
field_create_field(array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'attributes' => array(
          'target' => 'default',
          'class' => '',
          'rel' => '',
          ),
        'url' => 0,
        'title' => 'optional',
        'title_value' => '',
        'enable_tokens' => 1,
        'display' => array(
          'url_cutoff' => 80,
          ),
        ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_failure_redirect' => array(
                'url' => 'field_failure_redirect_url',
                'title' => 'field_failure_redirect_title',
                'attributes' => 'field_failure_redirect_attributes',
                ),
              ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_failure_redirect' => array(
                'url' => 'field_failure_redirect_url',
                'title' => 'field_failure_redirect_title',
                'attributes' => 'field_failure_redirect_attributes',
                ),
              ),
            ),
          ),
        ),
        'foreign keys' => array(),
        'indexes' => array(),
        'field_name' => 'field_failure_redirect',
        'type' => 'link_field',
        'module' => 'link',
        'active' => '1',
        'locked' => '0',
        'cardinality' => '1',
        'deleted' => '0',
        'columns' => array(
            'url' => array(
              'type' => 'varchar',
              'length' => 2048,
              'not null' => FALSE,
              'sortable' => TRUE,
              ),
            'title' => array(
              'type' => 'varchar',
              'length' => 255,
              'not null' => FALSE,
              'sortable' => TRUE,
              ),
            'attributes' => array(
              'type' => 'text',
              'size' => 'medium',
              'not null' => FALSE,
              ),
            ),
        'bundles' => array(
            'node' => array(
              'xauth_path',
              ),
            ),
        ));
field_create_instance(array(
      'label' => 'Failure Redirect Path',
      'widget' => array(
        'weight' => '2',
        'type' => 'link_field',
        'module' => 'link',
        'active' => 0,
        'settings' => array(),
        ),
      'settings' => array(
        'url' => 0,
        'title' => 'none',
        'title_value' => '',
        'display' => array(
          'url_cutoff' => '80',
          ),
        'attributes' => array(
          'target' => 'default',
          'rel' => '',
          'class' => '',
          ),
        'enable_tokens' => 1,
        'user_register_form' => FALSE,
        ),
      'display' => array(
          'default' => array(
            'label' => 'above',
            'type' => 'default',
            'settings' => array(),
            'module' => 'link',
            'weight' => 4,
            ),
          'teaser' => array(
            'type' => 'hidden',
            'label' => 'above',
            'settings' => array(),
            'weight' => 0,
            ),
          ),
      'required' => 0,
      'description' => '',
      'default_value' => NULL,
      'field_name' => 'field_failure_redirect',
      'entity_type' => 'node',
      'bundle' => 'xauth_path',
      'deleted' => '0',
      ));
/*
//field_user_agent_hash:
field_create_field(array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'allowed_values' => array(
          '',
          '',
          ),
        'allowed_values_function' => '',
        ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_user_agent_hash' => array(
                'value' => 'field_user_agent_hash_value',
                ),
              ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_user_agent_hash' => array(
                'value' => 'field_user_agent_hash_value',
                ),
              ),
            ),
          ),
        ),
      'foreign keys' => array(),
      'indexes' => array(
          'value' => array(
            'value',
            ),
          ),
      'field_name' => 'field_user_agent_hash',
      'type' => 'list_boolean',
      'module' => 'list',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'columns' => array(
          'value' => array(
            'type' => 'int',
            'not null' => FALSE,
            ),
          ),
      'bundles' => array(
          'node' => array(
            'xauth_path',
            ),
          ),
      ));
field_create_instance(array(
      'label' => 'Include user agent in hash?',
      'widget' => array(
        'weight' => '3',
        'type' => 'options_onoff',
        'module' => 'options',
        'active' => 1,
        'settings' => array(
          'display_label' => 1,
          ),
        ),
      'settings' => array(
        'user_register_form' => FALSE,
        ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'list_default',
          'settings' => array(),
          'module' => 'list',
          'weight' => 2,
          ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(),
          'weight' => 0,
          ),
        ),
      'required' => 0,
      'description' => '',
      'default_value' => array(
          array(
            'value' => 0,
            ),
          ),
      'field_name' => 'field_user_agent_hash',
      'entity_type' => 'node',
      'bundle' => 'xauth_path',
      'deleted' => '0',
      ));
*/
//field_landing_path_shortkey:
field_create_field(array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
        ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_landing_path_shortkey' => array(
                'value' => 'field_landing_path_shortkey_value',
                'format' => 'field_landing_path_shortkey_format',
                ),
              ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_landing_path_shortkey' => array(
                'value' => 'field_landing_path_shortkey_value',
                'format' => 'field_landing_path_shortkey_format',
                ),
              ),
            ),
          ),
        ),
        'foreign keys' => array(
            'format' => array(
              'table' => 'filter_format',
              'columns' => array(
                'format' => 'format',
                ),
              ),
            ),
        'indexes' => array(
            'format' => array(
              'format',
              ),
            ),
        'field_name' => 'field_landing_path_shortkey',
        'type' => 'text',
        'module' => 'text',
        'active' => '1',
        'locked' => '0',
        'cardinality' => '1',
        'deleted' => '0',
        'columns' => array(
            'value' => array(
              'type' => 'varchar',
              'length' => '255',
              'not null' => FALSE,
              ),
            'format' => array(
              'type' => 'varchar',
              'length' => 255,
              'not null' => FALSE,
              ),
            ),
        'bundles' => array(
            'node' => array(
              'xauth_path',
              ),
            ),
        ));
field_create_instance(array(
      'label' => 'Short Key',
      'widget' => array(
        'weight' => '4',
        'type' => 'text_textfield',
        'module' => 'text',
        'active' => 1,
        'settings' => array(
          'size' => '255',
          ),
        ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
        ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'text_default',
          'settings' => array(),
          'module' => 'text',
          'weight' => 3,
          ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(),
          'weight' => 0,
          ),
        ),
      'required' => 0,
      'description' => 'human readable id for use in the landing_path',
      'default_value' => NULL,
      'field_name' => 'field_landing_path_shortkey',
      'entity_type' => 'node',
      'bundle' => 'xauth_path',
      'deleted' => '0',
      ));
//field_xauth_secret_key:
field_create_field(array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'max_length' => '255',
        ),
      'storage' => array(
        'type' => 'field_sql_storage',
        'settings' => array(),
        'module' => 'field_sql_storage',
        'active' => '1',
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_xauth_secret_key' => array(
                'value' => 'field_xauth_secret_key_value',
                'format' => 'field_xauth_secret_key_format',
                ),
              ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_xauth_secret_key' => array(
                'value' => 'field_xauth_secret_key_value',
                'format' => 'field_xauth_secret_key_format',
                ),
              ),
            ),
          ),
        ),
        'foreign keys' => array(
            'format' => array(
              'table' => 'filter_format',
              'columns' => array(
                'format' => 'format',
                ),
              ),
            ),
        'indexes' => array(
            'format' => array(
              'format',
              ),
            ),
        'field_name' => 'field_xauth_secret_key',
        'type' => 'text',
        'module' => 'text',
        'active' => '1',
        'locked' => '0',
        'cardinality' => '1',
        'deleted' => '0',
        'columns' => array(
            'value' => array(
              'type' => 'varchar',
              'length' => '255',
              'not null' => FALSE,
              ),
            'format' => array(
              'type' => 'varchar',
              'length' => 255,
              'not null' => FALSE,
              ),
            ),
        'bundles' => array(
            'node' => array(
              'xauth_path',
              ),
            ),
        ));
field_create_instance(array(
      'label' => 'Secret Key',
      'widget' => array(
        'weight' => '5',
        'type' => 'text_textfield',
        'module' => 'text',
        'active' => 1,
        'settings' => array(
          'size' => '60',
          ),
        ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
        ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'text_default',
          'settings' => array(),
          'module' => 'text',
          'weight' => 6,
          ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(),
          'weight' => 0,
          ),
        ),
      'required' => 1,
      'description' => 'The key shared with the sister site used to salt the hash',
      'default_value' => NULL,
      'field_name' => 'field_xauth_secret_key',
      'entity_type' => 'node',
      'bundle' => 'xauth_path',
      'deleted' => '0',
      ));
