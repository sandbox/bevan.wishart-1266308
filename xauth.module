<?php

/*
 *
 * Implements of hook_menu
 *
 */
function xauth_menu() {

  $items = array();

  $items['xa'] = array(
      'title' => t('XAuth Module'),
      'description' => t('dotCMS to Drupal SSO'),
      'page callback' => 'xauth_login',
      'access callback' => TRUE, 
      'type' => MENU_CALLBACK
      );
  $items['xa/export_fields'] = array(
      'title' => t('XAuth Export Fields'),
      'description' => t('Export xAuths content fields'),
      'page callback' => 'xauth_export_fields',
      'access callback' => TRUE, 
      'type' => MENU_CALLBACK
      );
  $items['xa/provide_url'] = array(
      'title' => t('provide_login_url'),
      'description' => t('generates a valid login url for xAuth, during development'),
      'page callback' => 'xauth_provide_url',
      'page arguments' => array('username','session_key','short_key'),
      'access callback' => TRUE, 
      'type' => MENU_CALLBACK
      );
  $items['admin/config/system/xauth'] = array(
    'title' => t('xAuth module settings'),
    'description' => t('setting global settings for the xAuth module'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xauth_admin'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'management',
    );


  return $items;
}

function xauth_node_view_alter(&$build) {
  $build['#post_render'] = array('xauth_path_post_render');
}

function xauth_path_post_render($content, $element) {
  switch($element['#node']->type) {
    case 'xauth_path':

      //drupal_set_message(nl2br(str_replace(' ','&nbsp;',print_r($element, 1))));
      $form = drupal_get_form('xauth_path_sample_link_form');
      $short_key_field = field_get_items('node', $element['#node'], 'field_landing_path_shortkey', $element['#node']->language);

      $form['short_key']['#value'] = $short_key_field[0]['value']; 
      $form_rendered = drupal_render($form);

      $content .= $form_rendered; 
      break;
  }
  return $content;
}

function xauth_path_sample_link_form($form, &$form_state) {

  $form_title = t('Sample Links');

  $form = array();

  $form[$form_title] = array(
      '#type' => 'fieldset',
      '#title' => t('Generate sample links for this landing page'),
      '#weight' => 1,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      );

  $form[$form_title]['form help'] = array(
      '#markup' => t('To generate some sample login URLs as examples of what the external site must provide, enter a username and session_key from the external site and click build.')
      );

  $form[$form_title]['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#size' => 60,
      '#maxlength' => 60,
      '#description' => t('Please enter the username of a user from the external site.'),
      '#required' => TRUE,
      '#autocomplete_path' => 'user/autocomplete',
      );

  $form[$form_title]['session_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Session Key'),
      '#size' => 60,
      '#maxlength' => 60,
      '#description' => t('Please enter the session key associated with the supplied username on the external site.'),
      '#required' => TRUE,
      );

  $form[$form_title]['build'] = array(
      '#type' => 'submit',
      '#title' => t('Build'),
      '#size' => 60,
      '#maxlength' => 60,
      '#description' => t('Click here to build your links'),
      '#required' => TRUE,
      '#value' => t('Build'),
      );

  $form['short_key'] = array(
      '#type' => 'hidden',
      );

  if (isset($form_state['step'])) {

      $content = '<p>';

      $link =  xauth_provide_url ($form_state['values']['username'], $form_state['values']['session_key'], $form_state['values']['short_key']);
      $time = date('H:i:s');
      $content .= "Sample Link at $time<br />$link<br />";

      $timestamp = strtotime('-5 minutes');
      $link =  xauth_provide_url ($form_state['values']['username'], $form_state['values']['session_key'], $form_state['values']['short_key'], $timestamp);
      $time = date('H:i:s', $timestamp);
      $content .= "Sample Link at $time<br />$link <br />";

      $timestamp = strtotime('+5 minutes');
      $time = date('H:i:s', $timestamp);
      $link =  xauth_provide_url ($form_state['values']['username'], $form_state['values']['session_key'], $form_state['values']['short_key'], $timestamp);
      $content .= "Sample Link at $time<br />$link <br />";

      $content .= '</p>';

      $form[$form_title]['results'] = array(
          '#type' => 'fieldset',
          '#title' => t('Sample Links'),
          '#weight' => 1,
          '#collapsible' => FALSE,
          '#collapsed' => FALSE,
          );

      $form[$form_title]['results']['result_marker'] = array(
          '#markup' => $content, 
          );

  }

  return $form;

}

function xauth_path_sample_link_form_submit(&$form, &$form_state) {

  $form_state['step'] = true;
  $form_state['rebuild'] = true;

  return;

}

function xauth_admin() {
  $form = array();

  $form['xauth_invalid_shortkey_redirect'] = array(
      '#type' => 'textfield',
      '#title' => t('Invalid Shortkey Redirect'),
      '#default_value' => variable_get('xauth_invalid_shortkey_redirect'),
      '#size' => 60,
      '#maxlength' => 60,
      '#description' => t('This is the URL that the user will be redirected to in the case that the shortkey on the incoming URL is invalid. example: http://www.catalyst-au.net/'),
      '#required' => TRUE,
      );

  $form['xauth_default_hash_time_block'] = array(
      '#type' => 'textfield',
      '#title' => t('Default hash time block'),
      '#default_value' => variable_get('xauth_default_hash_time_block'),
      '#size' => 2,
      '#maxlength' => 2,
      '#description' => t('This is the number used to specify what time frame the current time is rounded to when generating the hash. eg. if set to 5, 14:56:19 will become 14:55:00'),
      '#required' => TRUE,
      '#field_suffix' => 'minutes',
      );

  $form['xauth_default_standard_user_login'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow login of standard drupal users'),
      '#default_value' => variable_get('xauth_default_standard_user_login'),
      '#description' => t('Check this if you wish to allow users created by drupal to login with the xauth module.'),
      );

  return system_settings_form($form);
}

function xauth_form_xauth_admin_alter (&$form, &$form_state) {
  //drupal_set_message(nl2br(str_replace(' ','&nbsp;',print_r($form, 1))));
  //$form['xauth_invalid_shortkey_redirect']['#element_validate'] = array('xauth_invalid_shortkey_redirect_validate');
}

function xauth_admin_validate($form, &$form_state) {

  //test that xauth_invalid_shortkey_redirect begins with http://
  if ('http://' != strtolower(substr($form_state['values']['xauth_invalid_shortkey_redirect'] , 0 ,  7))) {
    $form_state['values']['xauth_invalid_shortkey_redirect'] = 'http://'.$form_state['values']['xauth_invalid_shortkey_redirect'];
}

if (!is_numeric ( $form_state['values']['xauth_default_hash_time_block'] )) {
  form_set_error('xauth_default_hash_time_block', t('Default hash time block must be a number greater than 5 and less than 60 minutes'));
} else if ($form_state['values']['xauth_default_hash_time_block'] < 5 || $form_state['values']['xauth_default_hash_time_block'] > 60 ) {
  form_set_error('xauth_default_hash_time_block', t('Default hash time block must be a number greater than 5 and less than 60 minutes'));
}

}


function xauth_node_validate($node, $form, &$form_state) {

  switch ($node->type) {

    case 'xauth_path':
      //Enforce uniqueness on the shortkey 

      //store the shortkey from the form state, for easy access
      $shortkey = $form_state['values']['field_landing_path_shortkey']['und'][0]['value'];

      //set a default query to find the shortkey as an existing value in the database
      $query = 'SELECT entity_id FROM {field_data_field_landing_path_shortkey} WHERE entity_type = \'node\' AND bundle = \'xauth_path\' AND field_landing_path_shortkey_value = :shortkey';
      $query_parameters = array(':shortkey' => $shortkey);

      //if $node->nid is empty, this is a new node we are creating so use the default query
      //if it is set, we are saving the node, so DO NOT check this nodes shortkey value
      if (!empty($node->nid)) {
        $query .= ' AND entity_id != :node_id';
        $query_parameters[':node_id'] = $node->nid; 
      }

      //if we found a result from the query.. fail
      if (db_query($query, $query_parameters)->fetchField()) {
        form_set_error('landing_path_shortkey', t("The shortkey: $shortkey has already been used, please choose another."));
      } 
      break;
  }

}

function xauth_login($shortkey = FALSE, $username = FALSE, $session_key = FALSE, $hash = FALSE) {
  $secret_key = 'secret_key';

  //load relevant xauth_path based on supplied shortkey
  if ($xap_node = xauth_path_node_load($shortkey)) {

    $field_landing_path = field_get_items('node', $xap_node, 'field_landing_path', $langcode = NULL);
    $field_failure_redirect = field_get_items('node', $xap_node, 'field_failure_redirect', $langcode = NULL);
    $field_xauth_secret_key = field_get_items('node', $xap_node, 'field_xauth_secret_key', $langcode = NULL);
    $field_xauth_secret_key = $field_xauth_secret_key[0]['value'];

    if ($shortkey && $username && $session_key && $hash) {

      //check username and session_key against dotCMS 
      $valid_user_and_session = 1;

      if ($valid_user_and_session) {


        //compare a hash for 5 minutes either side of now
        $time = time();
        $rounded_time = xauth_minutes_round_down($time, variable_get('xauth_default_hash_time_block'));
        $message = "tested time: ".date('H:i:s', $rounded_time).'<br>';
        $hash_primary = substr(md5($username.$session_key.$rounded_time.$field_xauth_secret_key),0,10);

        //Valid dotCMS user.. check for existence of and/or create user with this username 
        //check hash against woodsolutions 
        $valid_hash = 0;
        if ($hash == $hash_primary) {
          $valid_hash = 1;
        } else {

          $time = strtotime(date('H:i:s', $time).' - 5 minutes');
          $rounded_time = xauth_minutes_round_down($time, variable_get('xauth_default_hash_time_block'));
          $message .= "tested time: ".date('H:i:s', $rounded_time).'<br>';
          $hash_secondary = substr(md5($username.$session_key.$rounded_time.$field_xauth_secret_key),0,10);

          if ($hash == $hash_primary) {
             $valid_hash = 1;
          }
        }


        $message .= "hash_primary: $hash_primary<br>"; 
        $message .= "hash_secondary: $hash_secondary<br>"; 
        $message .= "url hash: $hash<br>"; 


        if ($valid_hash) {

          //if xauth is configure to allow login of standard users
          //convert the logging in non external user, to an external user 
          if (variable_get('xauth_default_standard_user_login')) {
            if (!$account = user_external_load($username)) {
              if ($account = user_load_by_name($username)) {
                user_set_authmaps($account, array("authname_xauth" => $username));
              }
            }
          } else if (!$account = user_external_load($username)) {
            drupal_set_message("The user $username is not an external user, and xAuth module is not allowing non external logins, please contact a site administrator.", 'error');
            drupal_goto($field_failure_redirect[0]['url']);
          }

          //login the user
          //drupal_set_message('log in the user');
          user_external_login_register($username, 'xauth');
          //drupal_set_message($message);
          drupal_goto($field_landing_path[0]['url']);

          //return "success!! go to {$field_landing_path[0]['url']}";
        }

        drupal_set_message(t('Invalid login hash supplied, perhaps your login link has expired. Please contact a site administrator'),'error');
        drupal_goto($field_failure_redirect[0]['url']);

      } else {
        drupal_set_message(t('Invalid login credentials supplied. Please contact a site administrator'),'error');
        drupal_goto($field_failure_redirect[0]['url']);
      }

    }

    //return "failure!! go to {$field_failure_redirect[0]['url']}";
    drupal_set_message(t('Malformed login link supplied. Please contact a site administrator'),'error');
    drupal_goto($field_failure_redirect[0]['url']);

  }

  //no such landing page.. go to invalid landing page URL 
  drupal_set_message(t('Invalid landing page values (xAuth path shortkey) set in URL. Please contact a site administrator'),'error');
  drupal_goto(variable_get('xauth_invalid_shortkey_redirect'));

}


/* export and output to browser text area.. 
 * the xauth_path content type and 
 * the files of the xauth_path content type
 */
function xauth_export_fields() {

  include_once DRUPAL_ROOT . '/includes/utility.inc';
  $xauth_path = node_type_load('xauth_path');
  $xauth_path_exported = var_export($xauth_path, 1);
  drupal_set_message("Xauth path content type: <textarea rows=30 style=\"width: 100%;\">". $xauth_path_exported.'</textarea>');


  //$fields = array('field_landing_path','field_failure_redirect','field_user_agent_hash','field_landing_path_shotkey');
  $fields = array('field_landing_path','field_failure_redirect','field_landing_path_shortkey','field_xauth_secret_key');

  $output = '';
  foreach ($fields as $field_name) {
    $entity_type = 'node';
    $bundle_name = 'xauth_path';

    $info_config = field_info_field($field_name);
    $info_instance = field_info_instance($entity_type, $field_name, $bundle_name);
    unset($info_config['id']);
    unset($info_instance['id'], $info_instance['field_id']);
    $output .= "echo \"creating $field_name:\\n\";\n";
    $output .= "//$field_name:\nfield_create_field(" . drupal_var_export($info_config) . ");\n";
    $output .= "echo \"creating $field_name instance:\\n\";\n";
    $output .= "field_create_instance(" . drupal_var_export($info_instance) . ");\n";
  }
  drupal_set_message("xauth_path fields:<textarea rows=30 style=\"width: 100%;\">". $output .'</textarea>');

  return 'export complete';

}

/*
 * load an node of an xauth_path content based on the value of its shortkey field
 */
function xauth_path_node_load($shortkey) {

  $query = "SELECT entity_id FROM {field_data_field_landing_path_shortkey} WHERE entity_type = 'node' AND bundle = 'xauth_path'  AND field_landing_path_shortkey_value = :shortkey";
  $nid = db_query($query, array(':shortkey' => $shortkey))->fetchField();

  if ($nid) {
    return node_load( $nid );
  } else {
    echo 'false';
    exit;
    return 0;
  }

}

function xauth_provide_url ($username, $session_key, $short_key, $time = NULL) {
  global $base_root, $hour;

  if (empty($time)) {
    $time = time(); 
  }

  $xa_node = xauth_path_node_load($short_key);

  $field_xauth_secret_key = field_get_items('node', $xa_node, 'field_xauth_secret_key', $xa_node->language);
  $field_xauth_secret_key = $field_xauth_secret_key[0]['value'];

  $time_block = xauth_minutes_round_down ($time, variable_get('xauth_default_hash_time_block'));

  $hash = substr(md5($username.$session_key.$time_block.$field_xauth_secret_key),0,10) ;

  $path = "$base_root/xa/$short_key/$username/$session_key/$hash";


  return (l($path, $path));
}

/*
 * rounds the time down to the nearest block of minutes
 * $epoch_time = a unix timestamp
 * $minutes_block the block of time to round down to nearest of
 * 
 */
function xauth_minutes_round_down ($epoch_time, $minutes_block = '5') {
  $seconds_block = $minutes_block * 60; ;
  $rounded = ($epoch_time - $epoch_time%$seconds_block);
  return $rounded; 
}


function xauth_enable() {
  $message = array();
  $message[0] = 'xAuth Path module has been installed successfully';
  $message[1] = '1) Please go to '.l('xAuth Configuration Page','#overlay=admin/config/system/xauth').' and enter necessary configuration information.';
  $message[2] = '2) Create a new content of type xAuth path.';

  if(php_sapi_name() == 'cli') {

    $message[1] = '1) Please go to http://your-drupal-site/#overlay=admin/config/system/xauth and enter necessary configuration information.';
    echo urldecode(implode($message, "\n"))."\n";
  } else {
    //Not in cli-mode
    drupal_set_message(implode($message, '<br />'));
  }


}

